/* _____  _____  __  __             _____  _____ 
  / ____||_   _||  \/  |    /\     / ____||_   _|
 | |  __   | |  | \  / |   /  \   | (___    | |  
 | | |_ |  | |  | |\/| |  / /\ \   \___ \   | |  
 | |__| | _| |_ | |  | | / ____ \  ____) | _| |_ 
  \_____||_____||_|  |_|/_/    \_\|_____/ |_____|
  (c) 2017 GIMASI SA                                               

 * tuino_lora.ino
 *
 *  Created on: April 30, 2017
 *      Author: Massimo Santoli
 *      Brief: Example Sketch to use LoRa GMX-LR1 Module
 *      Version: 1.1
 *
 *      License: it's free - do whatever you want! ( provided you leave the credits)
 *
 */
 
#include "gmx_lr.h"
#include <Wire.h>
#include "Adafruit_TCS34725.h"

unsigned long timer_period_to_tx = 3 * 1000L;
unsigned long timer_millis_lora_tx = 0;
int ledState = 0;

bool data_received=false;


#define SR04_LEVEL_TRIGGER 3
#define SR04_LEVEL_ECHO    4
#define RGB_LED      5

#define SR04_TRIGGER 6
#define SR04_ECHO    7

#define BUZZER_PIN      13

#define BIN_ID 0x1234

#define OBJECT_NONE       0
#define OBJECT_BOTTLE     1
#define OBJECT_PLASTIC    2
#define OBJECT_CARDBOARD  3

#define DISTANCE_SENSOR A0

Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_50MS, TCS34725_GAIN_4X);

bool objectDetected = false;

// Declare the interrupt callback function for RX
void loraRx(){
  data_received = true;
}


void setup() {
  // put your setup code here, to run once:
  String DevEui;
  String AppEui;
  String AppKey;
  String _AppEui;
  String _AppKey;
  String loraClass;
 
  String adr,dcs,dxrate;

  byte join_status;
  int join_wait;
  
  Serial.begin(115200);
  Serial.println("Starting");

  pinMode(LED_BUILTIN,OUTPUT);
 
  pinMode(RGB_LED, OUTPUT);
  pinMode(BUZZER_PIN, OUTPUT);
  
  pinMode(SR04_TRIGGER, OUTPUT);
  pinMode(SR04_ECHO, INPUT);

  // GMX-LR init pass callback function
  gmxLR_init(&loraRx);

  // Set AppEui and AppKey 
  // Uncomment if you want to change from default settings
  // AppEui = "78:af:58:03:00:00:02:50";
  // AppKey = "78:af:58:03:00:00:02:50:78:af:58:03:00:00:02:50";

  Serial.println("Joining...");
  join_wait = 0; 
  while((join_status = gmxLR_isNetworkJoined()) != LORA_NETWORK_JOINED) {

  
    if ( join_wait == 0 )
    {
     // If AppKey and/or AppEUI are specified set them
     if (AppEui.length() > 0 )
      gmxLR_setAppEui(AppEui);
     if (AppKey.length() > 0 )
      gmxLR_setAppKey(AppKey);
    
      // Disable Duty Cycle  ONLY FOR DEBUG!
      gmxLR_setDutyCycle("0");
      gmxLR_setClass("C");

      Serial.println("LoRaWAN Params:");
      gmxLR_getDevEui(DevEui);
      Serial.println("DevEui:"+DevEui);
      gmxLR_getAppEui(_AppEui);
      Serial.println("AppEui:"+_AppEui);
      gmxLR_getAppKey(_AppKey);
      Serial.println("AppKey:"+_AppKey);
      gmxLR_getClass(loraClass);
      Serial.println("Class:"+loraClass);
      adr = String( gmxLR_getADR() );
      Serial.println("ADR:"+adr);
      dcs = String( gmxLR_getDutyCycle() );
      Serial.println("DCS:"+dcs);
      gmxLR_getRX2DataRate(dxrate);
      Serial.println("RX2 DataRate:"+dxrate);
      
      gmxLR_Join();
    }
    Serial.print("Join:");
    Serial.println(join_wait);
  
    join_wait++;

    // Not really necessary - but we reset everything after a timeout
    if (!( join_wait % 100 )) {
      gmxLR_Reset();
      join_wait = 0;
    }

    delay(5000);

  };
  
  Serial.println("Network Joined!");

  Serial.println("Color View Test!");
  while (!tcs.begin()) {
    Serial.println("No TCS34725 found ... check your connections");
    delay(1000);
  }
  
  Serial.println("Found sensor");


  Serial.println("========= SETUP DONE =========");
}

void loop() {
  // put your main code here, to run repeatedly:
  static int objectDetectCount = 0;
  static int lastDetectedObject = OBJECT_NONE;
  
  String rx_data;
  char rx_buf[128];
  int rx_buf_len;
  int port;


   if (data_received)
   {
      // retrive RX data from GMX-LR module. 
      // rx_data is a HEX String
      // port is also returned
      gmxLR_RXData(rx_data,&port);

      //convert HEX String to byte array for simpler packet decoding
      gmxLR_StringToHex(rx_data, rx_buf, &rx_buf_len );

      Serial.println("LORA RX DATA:"+rx_data);  
      Serial.print("LORA RX PORT:");
      Serial.println(port);

      if ( rx_buf[0] == 0x01 )
        digitalWrite(LED_BUILTIN,HIGH);
      else
        digitalWrite(LED_BUILTIN,LOW);
        
      // clear flag
      data_received = false;
   }

  int distance = 200;
  bool distanceValid = detectObject(&distance);
  
  bool objectDetectedNow = distanceValid && distance <= 5;

  if (objectDetectedNow && !objectDetected) {
    // objected is now detected for the first time
    setBuzz(1);
  }

  if (distanceValid) {
    objectDetected = objectDetectedNow;
  }

  if (objectDetected) {
    int object = scanObject();

    if (object == lastDetectedObject) {
      objectDetectCount++;
    } else {
      objectDetectCount = 0;
    }
    Serial.print("detect count = ");
    Serial.println(objectDetectCount);
    
    lastDetectedObject = object;
    
    if (object != OBJECT_NONE && objectDetectCount == 3) {
      transmitObjectDetected(object);
      setBuzz(2);
    }
  }

  if (distanceValid && !objectDetectedNow && objectDetectCount != 0) {
    Serial.println("reset detect count =================");
    objectDetectCount = 0;
  }

  int val;
  //scanFillLevel(&val);
}

void setBuzz(int mode) {
  if (mode == 2) {
    digitalWrite(BUZZER_PIN, HIGH);
    delay(750);
    digitalWrite(BUZZER_PIN, LOW);
  }
}

void transmitObjectDetected(int object) {
  static long int delta_lora_tx;
  String RSSI;
  String SNR;
  int _snr;
  
   delta_lora_tx = millis() - timer_millis_lora_tx;
   
     // Transmit Period 
   // check delta TX Timeout
   if ( delta_lora_tx > timer_period_to_tx) {
    Serial.println("TX DATA");

    String msg = "313B";         // bin Id = 1
    msg.concat(getString(50)); // fillLevel
    msg.concat("3B");   // ;
    msg.concat(getString(object)); // detectedObject

    Serial.print("msg = ");
    Serial.println(msg);

    // Transmit Data - as HEX String
    gmxLR_TXData(msg);
    
    // get RSSI and SNR of last received packet
    gmxLR_getRSSI(RSSI);
    Serial.println("RSSI: "+RSSI);
    gmxLR_getSNR(SNR);
    _snr = SNR.toInt();
    Serial.print("SNR: ");
    Serial.println(_snr);

     timer_millis_lora_tx = millis();
   } else {
    Serial.println("do not send to server");
   }
}

String getString(int value) {
  if (value == 0) {
    return "0";
  }
  
  String out = "";
  int digit;

  if (value >= 10000) {
    digit = (value / 10000) % 10;
    out.concat(3);
    out.concat(digit);
  }

  if (value >= 1000) {
    digit = (value / 1000) % 10;
    out.concat(3);
    out.concat(digit);
  }
  
  if (value >= 100) {
    digit = (value / 100) % 10;
    out.concat(3);
    out.concat(digit);
  }

  if (value >= 10) {
    digit = (value / 10) % 10;
    out.concat(3);
    out.concat(digit);
  }
  
  digit = (value / 1) % 10;
  out.concat(3);
  out.concat(digit);

  Serial.print("in = ");
  Serial.print(value);
  
  Serial.print("... out = ");
  Serial.println(out);
  return out;
}

bool scanFillLevel(int *distanceValue) {
  static long lastMeasure = 0;

  long now = millis();
  if (now - lastMeasure < 600) {
    return false;
  }
  lastMeasure = now;

  Serial.print("bin fill \t\t\t");
  return measureDistance(distanceValue, SR04_LEVEL_TRIGGER, SR04_LEVEL_ECHO);
}

bool detectObject(int *distanceValue) {
  static long lastMeasure = 0;

  long now = millis();
  if (now - lastMeasure < 200) {
    return false;
  }
  lastMeasure = now;

  Serial.print("object \t\t");

  float volts = analogRead(DISTANCE_SENSOR)*0.0048828125;  // value from sensor * (5/1024)
  int distance = 13*pow(volts, -1); // worked out from datasheet graph
  
  Serial.println(distance);   // print the distance

  *distanceValue = distance;
  return true;
}

bool measureDistance(int *distanceValue, int pinTrig, int pinEcho) {
  
  long duration, distance;
  digitalWrite(SR04_LEVEL_TRIGGER, LOW);  // Added this line
  delayMicroseconds(2); // Added this line
  digitalWrite(SR04_LEVEL_TRIGGER, HIGH);
  delayMicroseconds(10); // Added this line
  digitalWrite(SR04_LEVEL_TRIGGER, LOW);
  
  duration = pulseIn(SR04_LEVEL_ECHO, HIGH);
  distance = (duration/2) / 29.1;
  
  if (distance >= 200 || distance <= 0){
    Serial.println("Out of range");
    *distanceValue = 200;
    return true;
  } else {
    Serial.print(distance);
    Serial.println(" cm");
    *distanceValue = distance;
    return true;
  }
}

int scanObject() {
  uint16_t clear, red, green, blue;

  // turn LED on
  digitalWrite(RGB_LED, HIGH);
  
  delay(60);  // takes 50ms to read 
  tcs.getRawData(&red, &green, &blue, &clear);

  // turn LED off
  digitalWrite(RGB_LED, LOW);

  uint16_t temp = tcs.calculateColorTemperature(red, green, blue);
  
  Serial.print("C:\t"); Serial.print(clear);
  Serial.print("\tR:\t"); Serial.print(red);
  Serial.print("\tG:\t"); Serial.print(green);
  Serial.print("\tB:\t"); Serial.print(blue);
  Serial.print("\tT:\t"); Serial.print(temp);

  int object = detectObject(clear, red, green, blue);
  Serial.print("\t");
  printObject(object);
  Serial.println("");

  return object;
}

void printObject(int object) {
  switch (object) {
    case OBJECT_NONE:
      Serial.print("detection_in_progress");
      break;
    case OBJECT_BOTTLE:
      Serial.print("bottle");
      break;
    case OBJECT_PLASTIC:
      Serial.print("plastic");
      break;
    case OBJECT_CARDBOARD:
      Serial.print("cardboard");
      break;
    default:
      Serial.print("## unknown_object ##");
      break;
  }
}

int detectObject(uint16_t clear, uint16_t red, uint16_t green, uint16_t blue) {
  float redGreen  = (float)red   / (float)green;
  float redBlue   = (float)red   / (float)blue;
  float greenBlue = (float)green / (float)blue;

  Serial.println("");
  Serial.print("\t\trg:\t");
  Serial.print(redGreen);
  Serial.print("\trb:\t");
  Serial.print(redBlue);
  Serial.print("\tgb:\t");
  Serial.print(greenBlue);
//  Serial.println("");
  
  if (redGreen < 0.95 && greenBlue > 1.35) {
    return OBJECT_BOTTLE;
  } 
  if (redGreen < 1 && greenBlue > 1.55) {
    return OBJECT_BOTTLE;
  }

  if (redGreen > 1.6 && redBlue > 1.6) {
    return OBJECT_PLASTIC;
  } 

  if (redGreen > 1.2 && greenBlue > 1.25) {
    return OBJECT_CARDBOARD;
  }

  return OBJECT_NONE;
}

