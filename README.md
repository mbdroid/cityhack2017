Devices used:

- Gimasi Tuino One
- - http://www.tuino.io/
- - https://github.com/gimasi/TUINO_ONE
- - library: https://github.com/gimasi/TUINO_ONE/tree/master/tuino_libs/gmx/gmx_lr
- - LoRa Dashboard: https://dev1.thingpark.com/thingpark/wlogger/gui/

- Senzor de culoare RGB cu filtru IR - TCS34725
- - https://www.robofun.ro/senzor-de-culoare-rgb-cu-filtru-ir-tcs34725
- - https://www.adafruit.com/product/1334
- - https://learn.adafruit.com/adafruit-color-sensors/overview
- - library: https://github.com/adafruit/Adafruit_TCS34725

- Sharp GP2Y0A51SK0F Analog Distance Sensor 2-15cm
- - https://www.pololu.com/product/2450

- Buzzer
